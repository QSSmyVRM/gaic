﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using cryptography;


namespace myVRM.DataLayer
{
    /// <summary>
    /// Summary description for EWS_ServerConfig_D and related tables
    /// </summary>
    ///
    public class vrmEWSConfig
    {

        #region Private Internal Members

        private string m_EWSURL, m_EWSVersion;
        private string m_EWSUsername;
        private string m_EWSPassword;
        private int m_OrgId;
        private int m_UID;


        #endregion

        #region Public Properties

        public string EWSURL
        {
            get { return m_EWSURL; }
            set { m_EWSURL = value; }
        }
        public string EWSUsername
        {
            get { return m_EWSUsername; }
            set { m_EWSUsername = value; }
        }
        public string EWSPassword
        {
            get { return m_EWSPassword; }
            set { m_EWSPassword = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public string EWSVersion
        {
            get { return m_EWSVersion; }
            set { m_EWSVersion = value; }
        }
        #endregion
    }
}


       
    

    

