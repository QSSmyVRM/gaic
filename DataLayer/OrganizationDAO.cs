﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for Departments.
    /// Implement IDispose to close session
    /// </summary>
    public class orgDAO : vrmDAO
    {
        public orgDAO(string config, log4net.ILog log) : base(config, log) { }

        
        public class OrgDao :
               AbstractPersistenceDao<vrmOrganization, int>, IOrgDAO
        {
            public OrgDao(string ConfigPath) : base(ConfigPath) { }

        }
        public IOrgDAO GetOrgDao() { return new OrgDao(m_configPath); }


        #region OrgSettingsDAO

        public class OrgSettingsDAO :
               AbstractPersistenceDao<OrgData, int>, IOrgSettingsDAO
        {
            public OrgSettingsDAO(string ConfigPath) : base(ConfigPath) { }

            #region GetByOrgId
            /// <summary>
            /// GetByOrgId
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public OrgData GetByOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", id));
                    List<OrgData> orgData = GetByCriteria(criterionList);

                    if (orgData.Count <= 0)
                        throw new Exception("Organization id not found");

                    return orgData[0];
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion
        }

        public IOrgSettingsDAO GetOrgSettingsDao() { return new OrgSettingsDAO(m_configPath); }
                
        #endregion

        #region SysApproverDAO

        public class SysApproverDAO :
               AbstractPersistenceDao<sysApprover, int>, ISysApproverDAO
        {
            public SysApproverDAO(string ConfigPath) : base(ConfigPath) { }

            public IList<sysApprover> GetSysApproversByOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", id));
                    IList<sysApprover> mySysApp = GetByCriteria(criterionList);
                    return mySysApp;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public ISysApproverDAO GetSysApproverDao() { return new SysApproverDAO(m_configPath); }
        #endregion

        #region vrmSysTechDAO

        public class vrmSysTechDAO :
              AbstractPersistenceDao<sysTechData, int>, ISysTechDAO
        {
            public vrmSysTechDAO(string ConfigPath) : base(ConfigPath) { }
            public sysTechData GetTechByOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", id));
                    List<sysTechData> mySysTech = GetByCriteria(criterionList);

                    if (mySysTech.Count < 1)
                        throw new Exception("Id not found");
                    return mySysTech[0];
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public ISysTechDAO GetSysTechDao() { return new vrmSysTechDAO(m_configPath); }
        #endregion

        #region DiagnosticsDAO

        public class DiagnosticsDAO :
              AbstractPersistenceDao<Diagnostics, int>, IDiagnosticDAO
        {
            public DiagnosticsDAO(string ConfigPath) : base(ConfigPath) { }

            public Diagnostics GetDiagnosticByOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", id));
                    List<Diagnostics> myDiagnostic = GetByCriteria(criterionList);

                    if (myDiagnostic.Count <= 0)
                        return null;//throw new Exception("Id not found");
                    
                    return myDiagnostic[0];
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public IDiagnosticDAO GetDiagnosticDAO() { return new DiagnosticsDAO(m_configPath); }

        #endregion

        #region HolidaysDAO
        /// <summary>
        /// FB 1861
        /// </summary>

        public class HolidaysDAO :
               AbstractPersistenceDao<holidays, int>, IHolidaysDAO
        {
            public HolidaysDAO(string ConfigPath) : base(ConfigPath) { }

            public IList<holidays> GetHolidaysByOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", id));
                    addOrderBy(Order.Asc("HolidayType"));
                    IList<holidays> myHolidays = GetByCriteria(criterionList);
                    return myHolidays;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public IHolidaysDAO GetHolidaysDao() { return new HolidaysDAO(m_configPath); }


        public class HolidaysTypeDAO :
              AbstractPersistenceDao<holidaysType, int>, IHolidaysTypeDAO
        {
            public HolidaysTypeDAO(string ConfigPath) : base(ConfigPath) { }

            public holidaysType GetHolidayTypebyID(int id)
            {
                holidaysType myHolidayType = null;
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("HolidayType", id));
                    List<holidaysType> myHolidaysTypes = GetByCriteria(criterionList);

                    if (myHolidaysTypes.Count > 0)
                        myHolidayType = myHolidaysTypes[0];

                    return myHolidayType;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public IHolidaysTypeDAO GetHolidaysTypeDAO() { return new HolidaysTypeDAO(m_configPath); }

        #endregion

        //FB 2154
        #region EmailDomainDAO
        public class EmailDomainDAO :
            AbstractPersistenceDao<vrmEmailDomain, int>, IEmailDomainDAO
        {
            public EmailDomainDAO(string ConfigPath) : base(ConfigPath) { }
            public List<vrmEmailDomain> GetEmailDomainOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("orgId", id));
                    List<vrmEmailDomain> EDomain = GetByCriteria(criterionList);
                    return EDomain;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public List<vrmEmailDomain> GetActiveEmailDomainbyOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("orgId", id));
                    criterionList.Add(Expression.Eq("Active", 1));
                    List<vrmEmailDomain> EDomain = GetByCriteria(criterionList);
                    return EDomain;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            //Zd 101865 starts
            public List<vrmEmailDomain> GetOrgIdByEmailDomain(string Emaildomain)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("Emaildomain", Emaildomain));
                    List<vrmEmailDomain> EDomain = GetByCriteria(criterionList);
                    return EDomain;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            //ZD 101865 ends
        }
        public IEmailDomainDAO GetEmailDomainDAO() { return new EmailDomainDAO(m_configPath); }

        #endregion

        //FB 2337
        #region OrgLicAgreementDAO
        public class OrgLicAgreementDAO :
            AbstractPersistenceDao<VrmOrgLicAgreement, int>, IOrgLicAgreementDAO
        {
            public OrgLicAgreementDAO(string ConfigPath) : base(ConfigPath) { }
            public List<VrmOrgLicAgreement> GetOrgLicAgreementById(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("UID", id));
                    List<VrmOrgLicAgreement> OrgLicAgr = GetByCriteria(criterionList);
                    return OrgLicAgr;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public VrmOrgLicAgreement GetOrgLicAgreementByOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgID", id));
                    List<VrmOrgLicAgreement> OrgLicAgr = GetByCriteria(criterionList);
                    if (OrgLicAgr.Count > 0)
                        return OrgLicAgr[0];
                    else
                        return new VrmOrgLicAgreement();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
        public IOrgLicAgreementDAO GetOrgLicAgreementDAO() { return new OrgLicAgreementDAO(m_configPath); }

        #endregion

        //FB 2501  EM7 Starts
        #region EM7OrgSettingsDao

        public class EM7OrgSettingsDao :
               AbstractPersistenceDao<vrmEM7OrgSilo, int>, IEM7OrgSettingsDAO
        {
            public EM7OrgSettingsDao(string ConfigPath) : base(ConfigPath) { }

            public IList<vrmEM7OrgSilo> GetByEM7OrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgID", id));
                    IList<vrmEM7OrgSilo> EM7Settings = GetByCriteria(criterionList);
                    return EM7Settings;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public IEM7OrgSettingsDAO GetEM7Settings() { return new EM7OrgSettingsDao(m_configPath); }
        #endregion
		//FB 2501 EM7 Ends
        //FB 2599 - Start
        //FB 2262
        #region Vidyo Settings

        public class VrmVidyoSettingsDAO : AbstractPersistenceDao<VrmVidyoSettings, int>, IVrmVidyoSettingsDAO
        {
            public VrmVidyoSettingsDAO(string ConfigPath) : base(ConfigPath) { }
            public VrmVidyoSettings GetVidyoSettingsByOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("orgID", id));
                    List<VrmVidyoSettings> OrgVidyoSet = GetByCriteria(criterionList);
                    if (OrgVidyoSet.Count > 0)
                        return OrgVidyoSet[0];
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public IVrmVidyoSettingsDAO GetVidyoSettingsDAO() { return new VrmVidyoSettingsDAO(m_configPath); }
        #endregion
        //FB 2599 - End
        //ZD 101525
        public class vrmLDAPGroupDAO : AbstractPersistenceDao<vrmLDAPGroups, int>, ILDAPGroupsDAO
        {
            public vrmLDAPGroupDAO(string ConfigPath) : base(ConfigPath) { }
            public List<vrmLDAPGroups> GetLDAPGroupByOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", id));
                    List<vrmLDAPGroups> OrgLdap = GetByCriteria(criterionList);
                    if (OrgLdap.Count > 0)
                        return OrgLdap;
                    else
                        return new List<vrmLDAPGroups>();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public ILDAPGroupsDAO GetLDAPGroupDao() { return new vrmLDAPGroupDAO(m_configPath); }

		//ZD 101443 Starts
        //LDAP Fixes
        public class vrmLDAPConfigDAO : AbstractPersistenceDao<vrmLDAPConfig, int>, ILDAPConfigDAO
        {
            public vrmLDAPConfigDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmLDAPConfig GetLDAPConfigDetails(string ConfigPath)
            {
                vrmLDAPConfig sysLDAPConfig = null;
                try
                {
                    sysLDAPConfig = new vrmLDAPConfig();
                    SessionManagement.GetSession(ConfigPath);
                    ISession session = SessionManagement.GetSession(ConfigPath);
                    if (!session.IsConnected)
                        session.Reconnect();

                    IList LDAPConfigList = session.CreateCriteria(typeof(vrmLDAPConfig)).List();

                    if (LDAPConfigList.Count < 1)
                        //Empty DB issue FB 2164
                        return sysLDAPConfig;
                    //throw new Exception("No Records found");
                    foreach (vrmLDAPConfig LDAPCnfg in LDAPConfigList)
                    {
                        sysLDAPConfig = LDAPCnfg;
                    }
                    return sysLDAPConfig;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public ILDAPConfigDAO GetLDAPConfigDao() { return new vrmLDAPConfigDAO(m_configPath); }
		//ZD 101443 End

        //ALLDEV-833 START
        #region vrmEWSConfig
        public class EWSConfigDAO : AbstractPersistenceDao<vrmEWSConfig, int>, IEWSConfigDAO
        {
            public EWSConfigDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmEWSConfig GetByEWSOrgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", id));
                    List<vrmEWSConfig> EWSSettings = GetByCriteria(criterionList);
                    if (EWSSettings.Count > 0)
                        return EWSSettings[0];
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public IEWSConfigDAO GetEWSSettings() { return new EWSConfigDAO(m_configPath); }
        #endregion
        //ALLDEV-833 END


        //ZD 101835
        public IConfArchiveSettingsDao GetConfArchiveSettingsDao() { return new ConfArchiveSettingsDao(m_configPath); }
        public IConfArchiveDetailsDao GetConfArchiveDetailsDao() { return new ConfArchiveDetailsDao(m_configPath); }

        public class ConfArchiveSettingsDao :
          AbstractPersistenceDao<vrmConfArchiveSettings, int>, IConfArchiveSettingsDao
        {
            public ConfArchiveSettingsDao(string ConfigPath) : base(ConfigPath) { }

        }

        public class ConfArchiveDetailsDao :
          AbstractPersistenceDao<vrmConfArchiveDetails, int>, IConfArchiveDetailsDao
        {
            public ConfArchiveDetailsDao(string ConfigPath) : base(ConfigPath) { }

        }
    }
}
