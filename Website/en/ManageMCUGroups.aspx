﻿<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>

<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ManageMCUGroups" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>

<script language="javascript" type="text/javascript">
    
    var img = new Image();
    img.src = "../en/image/wait1.gif";

    function OpenDetails(gpName,mcustring) {
        document.getElementById("ViewGroupDetails").style.display = 'block';
        document.getElementById("lblGroupName").innerText = gpName;

        $('#myTable').remove();
        var strhtml = "<table id='myTable' width='100%' cellpadding='0' cellspacing='0' style='border-width:thin;border-color:black;' >";
        strhtml += "<tr class='tableHeader' height='30px'>";
        strhtml += "<td><asp:Literal Text='<%$ Resources:WebResources, ResponseConference_MCUName%>' runat='server'></asp:Literal></td>";
        strhtml += "<td><asp:Literal Text='<%$ Resources:WebResources, Address%>' runat='server'></asp:Literal></td>";
        strhtml += "<td><asp:Literal Text='<%$ Resources:WebResources, Administrator%>' runat='server'></asp:Literal></td>";
        strhtml += "</tr>"
        if (mcustring != "") {
            var mcustrAry = mcustring.split("||");
            for (var i = 0; i < mcustrAry.length - 1; i++) {
                var mcusubstrAry = mcustrAry[i].split("!!");
                strhtml += "<tr class='tableBody'> <td>" + mcusubstrAry[0] + "</td><td>" + mcusubstrAry[1] + "</td><td>" + mcusubstrAry[2] + "</td></tr>";
            }
        }
        strhtml += "</table>";
        $("#divGpDetails").append(strhtml);
    }
    function fnShowHide(arg) {
        document.getElementById("ViewGroupDetails").style.display = 'none';
        return false;
    }
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block';
        else
            document.getElementById("dataLoadingDIV").style.display = 'none';
    }
   
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmManageMCUGroups" runat="server">
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <div id="ViewGroupDetails" runat="server" align="center" style="left: 250px; top: 150px;
        position: absolute; height: 350px; visibility: visible; z-index: 3; display: none;
        width: 725px">
        <table  cellpadding="5" cellspacing="5" style="border-color:Blue;border-width:1px;border-style:Solid; background-color:#E1E1E1;" class="tableBody" align="center" width="50%">
            <tr>
                <td align="center" class="subtitleblueblodtext">
                    <asp:Label ID="lblGroupName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="100px" valign="top" style="overflow-x:auto;"><%--ZD 100040--%>
                    <div id="divGpDetails" runat="server" style=" overflow:auto"></div>
                    <table runat="server" id="myTable" width="100%" cellpadding="0" cellspacing="0" style="border-width:thin;border-color:black;" >
                        <tr class="tableHeader" height="30px">
                            <td>MCU Name</td>
                            <td>Address</td>
                            <td>Administrator</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="BtnClose" Text="<%$ Resources:WebResources, ManageConference_BtnClose%>"
                        CssClass="altMedium0BlueButtonFormat" runat="server" OnClientClick="javascript:return fnShowHide('0');">
                    </asp:Button>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <input type="hidden" id="helpPage" value="73">
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="<%$ Resources:WebResources, ManageMCUGroup_ManageMCUGroups%>"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 100%">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <div id="dataLoadingDIV" style="display: none" align="center">
                    <img border='0' src='image/wait1.gif' alt='Loading..' />
                </div>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgGroups" runat="server" AutoGenerateColumns="False" CellPadding="2"
                        GridLines="None" AllowSorting="true" BorderColor="blue" OnItemDataBound="dgGroups_DataBound"
                        BorderStyle="solid" BorderWidth="1" ShowFooter="False" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteMCUGroup" OnEditCommand="EditMCUGroup" Width="70%" Visible="true"
                        Style="border-collapse: separate">
                        <SelectedItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <Columns>
                            <asp:BoundColumn DataField="groupID" Visible="false">
                                <HeaderStyle CssClass="tableHeader"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="groupName" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                HeaderText="<%$ Resources:WebResources, ManageGroup_GroupName%>"
                                HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="description" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                HeaderText="<%$ Resources:WebResources, ManageGroup_Description%>" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ManageTemplate_btnViewDetails%>"
                                ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <button id="btnViewDetails" runat="server" class="altMedium0BlueButtonFormat">
                                        <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageGroup_btnViewDetails%>"
                                            runat="server"></asp:Literal></button>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ManageGroup_Actions%>"
                                HeaderStyle-HorizontalAlign="center">
                                <HeaderStyle CssClass="tableHeader"></HeaderStyle>
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td align="center">
                                                <asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ManageGroup_btnEdit%>"
                                                    ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                            </td>
                                            <td align="center">
                                                <asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ManageGroup_btnDelete%>"
                                                    ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoGroups" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                               <asp:Literal Text="<%$ Resources:WebResources, ManageGroup_lblError%>" runat="server"></asp:Literal>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td height="50" colspan="3" align="center" valign="middle">
                    <br />
                    <img border="0" src="../image/aqualine.gif" alt="aqualine" width="200" height="2"
                        style="vertical-align: middle" />
                    <font color="#00CCFF" size="3"><b>
                        <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ManageGroup_OR%>" runat="server"></asp:Literal></b></font>
                    <img border="0" src="../image/aqualine.gif" alt="aqualine" width="200" height="2"
                        style="vertical-align: middle" />
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="5" width="100%">
                        <tr>
                            <td width="170" align="left">
                                &nbsp;
                            </td>
                            <td>
                                <span class="subtitleblueblodtext">
                                    <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ManageMCUGroup_SearchGroups%>"
                                        runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 1168px">
                    <table width="65%" align="center">
                        <tr>
                            <td align="center">
                                <table width="100%" align="center">
                                    <tr>
                                        <td align="left" class="blackblodtext" nowrap="">
                                            <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ManageGroup_GroupName%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                        <%-- FB 2921--%>
                                        <td align="left" width="350%">
                                            <asp:TextBox ID="txtSGroupName" runat="server" CssClass="altText" Style="margin-left: 0px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtSGroupName"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" class="blackblodtext" nowrap="">
                                            <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ManageMCUGroup_IncludedMCUs%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSMember" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSMember"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>"
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="blackblodtext">
                                            <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, ManageGroup_Description%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSDescription" TextMode="multiline" Rows="2" runat="server" CssClass="altText"
                                                Style="width: 200px; height: 36px;"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtSDescription"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <button id="btnReset" runat="server" onserverclick="ResetGroup" class="altMedium0BlueButtonFormat"
                                    onclick="javascript:DataLoading('1');">
                                    <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, Reset%>" runat="server"></asp:Literal></button>
                                <button id="btnSubmit" runat="server" onserverclick="SearchGroup" class="altMedium0BlueButtonFormat"
                                    onclick="javascript:DataLoading('1');">
                                    <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ManageGroup_btnSubmit%>"
                                        runat="server"></asp:Literal></button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="50" colspan="3" align="center" valign="middle">
                    <br />
                    <img border="0" src="../image/aqualine.gif" alt="aqualine" width="200" height="2"
                        style="vertical-align: middle" />
                    <font color="#00CCFF" size="3"><b>
                        <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ManageGroup_OR%>" runat="server"></asp:Literal></b></font>
                    <img border="0" src="../image/aqualine.gif" alt="aqualine" width="200" height="2"
                        style="vertical-align: middle" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table cellspacing="5" width="70%">
                        <tr>
                            <td width="170" align="left">
                                &nbsp;
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 1168px">
                    <table width="65%">
                        <tr>
                            <td align="right">
                                <button id="btnCreate" runat="server" onserverclick="CreateNewMCUGroup" class="altMedium0BlueButtonFormat"
                                    style="width: 25%;">
                                    <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ManageGroup_CreateNewGrou%>"
                                        runat="server"></asp:Literal></button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
    </form>
    <script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
